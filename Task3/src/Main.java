public class Main {

    public static void main(String[] args) {

        String[][] list = {
                {"Fahrenheit", "-20", "-10", "+0", "+20", "+30"},
                {"Celsius", "-28.89", "-23.33", "-17.78", "-6.67", "-1.11"}
        };
        int n = 0;
        do {
            System.out.printf("%-12s|%11s", list[0][n], list[1][n] + "\n");
            if (n == 0) {
                System.out.println("-----------------------");
            }
            n++;
        } while (n <= 5);

    }
}
