## Readme.md

recommended java version: 14\
Should be adjusted to older versions now


### Task 1

Done by simply printing the lines

### Task 2 

Done by two arrays and a do while loop

### Task 3

Done by a 2D array and a while loop with an if statement (probably more optimizable)


Available on: https://bitbucket.org/sziehnert/ls01_fic-01_ziehnert_rep