package com.company;

import java.util.Date;

public class Benutzer {

    private String name, passwort, email;
    private short permissions;
    private boolean loginStatus;
    private Date lastLogin = new Date();

    public Benutzer(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public short getPermissions() {
        return permissions;
    }

    public void setPermissions(short permissions) {
        this.permissions = permissions;
    }

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
        if (loginStatus) {
            setLastLogin(new Date());
        }

    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
}
