package com.company;

public class BenutzerTest {

    public static void main(String args[]) {
        var testBenutzer1 = new Benutzer("test1");
        var testBenutzer2 = new Benutzer("test2");
        var testBenutzer3 = new Benutzer("test3");

        testBenutzer1.setEmail("test@user1.test");
        testBenutzer1.setLoginStatus(true);
        testBenutzer1.setPasswort("Pa$$w0rd");
        testBenutzer1.setPermissions((short) 1);

        testBenutzer2.setEmail("test@user2.test");
        testBenutzer2.setLoginStatus(true);
        testBenutzer2.setPasswort("Pa$$w0rd");
        testBenutzer2.setPermissions((short) 2);

        testBenutzer3.setEmail("test@user3.test");
        testBenutzer3.setLoginStatus(true);
        testBenutzer3.setPasswort("Pa$$w0rd");
        testBenutzer3.setPermissions((short) 3);


        System.out.println("1. User");
        System.out.println("Name: " + testBenutzer1.getName());
        System.out.println("Passwort: " + testBenutzer1.getPasswort());
        System.out.println("Email: " + testBenutzer1.getEmail());
        System.out.println("Rechte: " + testBenutzer1.getPermissions());
        System.out.println("Letzter Login: " + testBenutzer1.getLastLogin());
        System.out.println("==============================");

        System.out.println("2. User");
        System.out.println("Name: " + testBenutzer2.getName());
        System.out.println("Passwort: " + testBenutzer2.getPasswort());
        System.out.println("Email: " + testBenutzer2.getEmail());
        System.out.println("Rechte: " + testBenutzer2.getPermissions());
        System.out.println("Letzter Login: " + testBenutzer2.getLastLogin());
        System.out.println("==============================");

        System.out.println("3. User");
        System.out.println("Name: " + testBenutzer3.getName());
        System.out.println("Passwort: " + testBenutzer3.getPasswort());
        System.out.println("Email: " + testBenutzer3.getEmail());
        System.out.println("Rechte: " + testBenutzer3.getPermissions());
        System.out.println("Letzter Login: " + testBenutzer3.getLastLogin());
        System.out.println("==============================");
    }
}
