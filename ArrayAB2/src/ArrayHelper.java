public class ArrayHelper {

    public static String convertArrayToString(int[] zahlen) {
        StringBuilder string = new StringBuilder();

        for (int i = 0; true; i++) {
            try {
                string.append(", ").append(zahlen[i]);
            } catch (Exception e) {
                break;
            }
        }

        return string.substring(2, string.length() - 2);
    }

    public static int[] reverseArray(int[] feld) {
        for (int i = 0; i < feld.length / 2; i++) {
            int x = feld[i];
            feld[i] = feld[feld.length - i - 1];
            feld[feld.length - i - 1] = x;
        }
        return feld;
    }

    public static int[] reverseArrayWithNewArray(int[] feld) {
        int[] neuesFeld = new int[7];
        for (int i = 0; i < feld.length / 2; i++) {
            int x = feld[i];
            feld[i] = feld[feld.length - i - 1];
            feld[feld.length - i - 1] = x;
            neuesFeld[i] = feld[i];
        }
        for (int i = 3; i < feld.length; i++) {
            neuesFeld[i] = feld[i];
        }
        return neuesFeld;
    }

    public static void celsiusToFahrenheit(int max) {
        double fahrenheit = 0.00;

        for (int i = 0; i <= max; i++, fahrenheit += 10) {
            if (i == 0) {
                System.out.printf("%-9s|%13s", "Celsius", "Fahrenheit" + "\n");
                System.out.println("----------------------");
            }
            double celsiusRounded = Math.ceil((fahrenheit - 32) * (0.5556) * 100) / 100;
            System.out.printf("%-9s|%13s", celsiusRounded, fahrenheit + "\n");

        }
    }
}
