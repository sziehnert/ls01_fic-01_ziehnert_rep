import java.util.Scanner;

public class Aufgabe4 {

    public static void main(String[] args) {

        int num;
        Scanner input = new Scanner(System.in);
        System.out.print("Länge der Tabelle angeben: ");
        num = input.nextInt();
        ArrayHelper.celsiusToFahrenheit(num);
    }
}
