public class Main {

    public static void main(String[] args) {

        String[] first = {"", "1", "1 * 2", "1 * 2 * 3", "1 * 2 * 3 * 4", "1 * 2 * 3 * 4 * 5"};
        String[] second = {"1", "1", "2", "6", "24", "120"};
        int i = 0;
        do {
            System.out.printf("%-5s", i + "!");
            System.out.print("= ");
            System.out.printf("%-18s", first[i]);
            System.out.print("=");
            System.out.printf("%4s\n", second[i]);
            i++;
        } while (i <= 5);
    }
}
